const express = require('express');
const { errorHandler } = require('../helpers/error');
const authenticationMiddleware = require('../middlewares/authentication');

const router = express.Router();

module.exports = function usersRouter(models) {
  const { User } = models;

  router.get(
    '/',
    authenticationMiddleware,
    errorHandler(async (req, res) => {
      const users = await User.find();
      res.render('pages/users/index', { users });
    })
  );

  router
    .route('/create')
    .get(
      authenticationMiddleware,
      errorHandler(async (req, res) => {
        res.render('pages/users/create');
      })
    )
    .post(
      authenticationMiddleware,
      errorHandler(async (req, res) => {
        const { firstname, lastname, email } = req.body;
        const user = new User({
          firstname,
          lastname,
          email,
        });
        await user.save();
        res.redirect('/users');
      })
    );

  router.get(
    '/:id',
      authenticationMiddleware,
      errorHandler(async (req, res) => {
      const { id } = req.params;
      const user = await User.findById(id);
      res.render('pages/users/show', { user });
    })
  );

  router
    .route('/:id/edit')
    .get(
      authenticationMiddleware,
      errorHandler(async (req, res) => {
        const { id } = req.params;
        const user = await User.findById(id);
        res.render('pages/users/edit', { user });
      })
    )
    .post(
      authenticationMiddleware,
      errorHandler(async (req, res) => {
        const { id } = req.params;
        const { firstname, lastname, email } = req.body;

        try {
          await User.updateOne(
            { _id: id },
            {
              firstname,
              lastname,
              email,
            }
          );
          res.redirect(`/users`);
        } catch (error) {
          res.send(422);
        }
      })
    );

  router.get(
    '/:id/delete',
    authenticationMiddleware,
    errorHandler(async (req, res) => {
      const { id } = req.params;
      await User.findById(id).remove();

      res.redirect('/users');
    })
  );

  return router;
};
