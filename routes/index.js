const express = require('express');
const jwt = require('jsonwebtoken');
const { errorHandler } = require('../helpers/error');
const { JWT_SECRET } = require('../constants/environment');

const router = express.Router();

module.exports = function indexRouter(models) {
  const { User } = models;

  /* GET home page. */
  router.get(
    '/',
    errorHandler(async (req, res) => {
      res.render('pages/index');
    })
  );

  router
    .route('/signup')
    .get(
      errorHandler(async (req, res) => {
        res.render('pages/users/signup');
      })
    )
    .post(
      errorHandler(async (req, res) => {
        const { firstname, lastname, email, password } = req.body;
        const user = new User({
          firstname,
          lastname,
          email,
        });
        user.setPassword(user, password);
        await user.save();
        res.redirect('/');
      })
    );


  /* POST home page. */
  router.post(
    '/',
    errorHandler( async (req, res) => {
      const { email } = req.body;
      const { password } = req.body;

      // todo test if email/password is OK
      // todo if email/password OK create jwt
      const user = await User.findOne({email});
      if(user) {
        try {
          if (user.validPassword(user, password)) {
            // todo add scopes for perm
            // exemple
            // const payload = {
            //   username: "toto",
            //   role:"admin" -> mongo
            // }
            const scopes = [user.role];
            // if role ADMIN then scopes = ADMIN, USER, MANAGER
            // if role USER then scopes = USER
            // if role MANAGER then scopes = USER, MANAGER

            const token = jwt.sign({ username: `${user.firstname} ${user.lastname}`, scopes }, JWT_SECRET, {expiresIn: '1d'});
            res.send(token);
          }
        } catch(e) {
          console.error(e);
        }
      }
      res.redirect('/');
    })
  );

  router.get('/logout', (req, res) => {
    res.redirect('/');
  });

  return router;
};
