/**
 * Error class for api calls
 */
class ApiError extends Error {
  constructor(message, statusCode) {
    super();
    this.statusCode = statusCode;
    this.message = message;
  }

  getStatusCode() {
    return this.statusCode;
  }
}

/**
 *
 * @param route
 * @returns {Function}
 */
function errorHandler(route) {
  return async (req, res, next) => {
    try {
      return await route(req, res, next);
    } catch (e) {
      return next(e);
    }
  };
}

module.exports = { errorHandler, ApiError };
