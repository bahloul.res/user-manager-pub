const createError = require('http-errors');
const express = require('express');
const logger = require('morgan');
const bodyParser = require('body-parser');
const path = require('path');
const mongoose = require('mongoose');

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const { ApiError } = require('./helpers/error');

require('dotenv').config();

const app = express();

mongoose
  .connect(process.env.DATABASE_URI)
  .catch(() => createError(503));

// Import all models
require('./models/index');

const { models } = mongoose;

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

// app.use(require('flash')());

/* Routes */
app.use('/', indexRouter(models));
app.use('/users', usersRouter(models));

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404));
});

// Error handler
// eslint-disable-next-line no-unused-vars
app.use((error, req, res, _next) => {
  console.log(error);

  if (error instanceof ApiError) {
    return res.status(error.getStatusCode()).json({ error: error.toString() });
  }
  return res.status(500).render('error', { error });
});

module.exports = app;
