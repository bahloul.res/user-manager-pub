const jwt = require('jsonwebtoken');

/**
 *
 * @param req
 * @param res
 * @param next
 */
const { JWT_SECRET } = require('../constants/environment');

function authentication(req, res, next) {
  try {
    const {authorization} = req.headers
    const token = authorization.split(' ')[1];
    jwt.verify(token, JWT_SECRET);
    next();
  } catch(e) {
    res.redirect('/');
  }
}

module.exports = authentication;
