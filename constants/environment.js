const STAGING = 'staging';
const PRODUCTION = 'production';
const { JWT_SECRET } = process.env;

module.exports = { STAGING, PRODUCTION, JWT_SECRET };
