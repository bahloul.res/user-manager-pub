# User Manager

# Installation
1. Install node modules :
```yarn install```

2. Create and fill `.env` file at the root's project :
```nano .env```

Copy/paste env var : 
```
SESSION_SECRET=what_ever_you_want
NODE_ENV=development
PORT=7000
USERNAME=what_ever_you_want
PASSWORD=what_ever_you_want
DATABASE_URI=mongodb://user-manager-db:27017/user_manager
```

3. Run project with docker compose
``` docker-compose up -d```

4. Verify your containers are running
```docker ps```

# Usefull commands
```
docker exec -it user-manager-db /bin/sh // go inside mongo db container
docker exec -it user-manager-api /bin/sh // go inside api container
docker logs user-manager-api -f  // logs (last) api container
```

