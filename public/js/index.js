const flashMessages = document.querySelectorAll(".flash-message i")

flashMessages && flashMessages.forEach(item => {
  item.addEventListener("click", function(e){
    e.target.parentElement.remove()
  })
})
