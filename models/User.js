const mongoose = require('mongoose');
const crypto = require('crypto');

const { Schema } = mongoose;
const { ObjectId } = Schema;

const UserSchema = new Schema({
  id: ObjectId,
  firstname: String,
  lastname: String,
  email: String,
  hash : String,
  salt : String,
  role: String,
});

UserSchema.methods.setPassword = (user, password) => {
  user.salt = crypto.randomBytes(16).toString('hex');
  user.hash = crypto.pbkdf2Sync(password, user.salt,
    1000, 64, `sha512`).toString(`hex`);

};

UserSchema.methods.validPassword = (user, password) => {
  const hash = crypto.pbkdf2Sync(password,
    user.salt, 1000, 64, `sha512`).toString(`hex`);
  return user.hash === hash;
};

module.exports = mongoose.model('User', UserSchema);


// > db.users.insert({firstname: "super admin", lastname: "ok", role: "ADMIN", email: "superadmin@example.com", hash: "db6b11a9e84020ccab89afdd615e10c4b9d00a4adcc7ae9b61087dbd1a5a2341b2f5e282ab051bb3213036da854cb3cb3bd1ecaaa8e7f1707707a08f3ab74d75", salt: "5eebc970841ee606d3e88ff47e315624"})
