const path = require('path');
const fs = require('fs');

const directoryPath = path.join(__dirname, '.');

const files = fs.readdirSync(directoryPath);

// Import all models in mongoose instance
module.exports = files
  .filter((file) => file !== 'index.js')
  .map((file) => {
    // eslint-disable-next-line import/no-dynamic-require,global-require
    require(`./${file.substring(0, file.length - 3)}`);
    return 0;
  });
